# Python Bloom Filter #

## Setup ##
* Clone the repository
* Install dependencies ([xxhash](https://pypi.python.org/pypi/xxhash/))
* Copy bloomfilter.py to your project folder
* Go to [http://devisedbydavid.com/open_source/bloom_filter](http://devisedbydavid.com/open_source/bloom_filter) for more information

## Usage ##
The following code example illustrates how to use this Bloom filter class in Python.

```
#!python

from bloomfilter import *

# A new bloom filter
bf = BloomFilter(1000, 0.01)
# Capacity: 1000
# False positive rate: 1%

# Adding an item to the filter
bf.add("Hello world!")

# True means the item is probably
# in the bloom filter
bf.check("Hello world!")
# True

# False means the item is definitely
# not in the bloom filter
bf.check("Hello world!!!")
# False
```

## Documentation ##
Please visit [http://devisedbydavid.com/open_source/bloom_filter](http://devisedbydavid.com/open_source/bloom_filter) for documentation.